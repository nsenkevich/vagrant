node 'madesimple.vm' {
  # Configure mysql
  class { 'mysql::server':
    root_password => '123'
  }
  class { 'mysql::bindings':
    php_enable =>true,
  }

  # Configure apache
  class { 'apache':  
      mpm_module => 'prefork',
    }

  apache::vhost { $::fqdn:
    port    => '80',
    docroot => '/var/www/test',
    require => File['/var/www/test'],
  }

  class { 'apache::mod::php': }

  # Configure Docroot and index.html
  file { ['/var/www', '/var/www/test']:
    ensure => directory
  }

  file { '/var/www/test/index.php':
    ensure  => file,
    content => '<?php echo \'<p>Hello World</p>\'; ?> ',
  }

  # Realize the Firewall Rule
  Firewall <||>

  #git
  include git

  # composer
  class { 'composer':
    command_name => 'composer',
    target_dir   => '/usr/local/bin'
  }
}